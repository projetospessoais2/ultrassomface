<div id="section4_1">
    <div class="row">
        <div class="col-md-1 col-lg-5"></div>
        <div class="col-md-10 col-lg-6 colClaudiaDesktop">
            <div class="row" id="rowSection4_1">
                <div class="col-1 col-sm-2 col-md-3"></div>
                <div class="col-10 col-sm-8 col-md-6 text-center text-white mt-4">
                    <img src="./assets/dra_claudia.webp" class="card-img" width="80%">
                </div>
                <div class="col-1 col-sm-2 col-md-3"></div>
            </div>
            
            <div class="row">
                <div class="col-1 col-sm-1 col-md-3"></div>
                <div class="col-10 col-sm-10 col-md-6 alignTextClaudia text-white mt-3 mb-3 textNormal">
        
                    Médica especialista em Ultrassonografia Músculo-Esquelética.
        
                    <div class="mt-2">
                        Conta com mais de 7 anos de experiência em métodos ligados à ultrassom dermatológica. 
                    </div>
        
                    <div class="mt-2">
                        Palestrante na Sociedade Brasileira de Ultrassom, Colégio Brasileiro de Radiologia, Colégio Brasileiro de Dermatologia e diversos congressos e eventos pelo Brasil.
                    </div>
        
                </div>
                <div class="col-1 col-sm-1 col-md-3"></div>
            </div>
        </div>
        <div class="col-md-1 col-lg-1"></div>
    </div>
</div>

<div id="section4_2">
    <div class="row">
        <div class="col-md-1 col-lg-1"></div>
        <div class="col-md-10 col-lg-6 mb-4 colJulianaDesktop">
            <div class="row" id="rowSection4_2">
                <div class="col-1 col-sm-2 col-md-3"></div>
                <div class="col-10 col-sm-8 col-md-6 text-center text-white mt-4">
                    <img src="./assets/dra_juliana.webp" class="card-img" width="80%">
                </div>
                <div class="col-1 col-sm-2 col-md-3"></div>
            </div>
            
            <div class="row">
                <div class="col-1 col-sm-1 col-md-3"></div>
                <div class="col-10 col-sm-10 col-md-6 alignTextJuliana text-white mt-3 mb-3 textNormal">

                    Médica Radiologista com atuação em Radiologia Pediátrica e Ultrassonografia Dermatológica.

                    <div class="mt-2">
                        Palestrante de diversas sociedades brasileiras como Sociedade Brasileiros de Ultrassonografia (SBUS), Colégio Brasileiro de Radiologia (CBR) e Sociedade Brasileira de Ultrassonografia Dermatológica (SBUD), bem como Sociedade Brasileira de Cirurgia Plástica (SBCP) e Sociedade Brasileira de Dermatologia (SBD), além de sociedades Internacionais como American Institute of Ultrassound (AIUM) e European Society or Radiology (ESR)
                    </div>

                    <div class="mt-2">
                        Autora de vários capítulos e artigos em ultrassonografia dermatológica.
                    </div>

                </div>
                <div class="col-1 col-sm-1 col-md-3"></div>
            </div>
        </div>
        <div class="col-md-1 col-lg-5"></div>
    </div>
</div>