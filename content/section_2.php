<div id="section2">

    <div class="row p-2">
        <div class="col-1 col-sm-1 col-md-2"></div>
        <div class="col-10 col-sm-10 col-md-8">
            <div class="text-center text-white mt-5 textNormal font-weight-light">
                Atue com ultrassom de alta resolução e tenha segurança para fazer ultrassom de face.
        
                <div class="font-weight-strong mt-3 textGiant">
                    COMO VAI FUNCIONAR?
                </div>
        
                <div class="mt-2">
                    A Masterclass Ultrassom de Face: da cosmiatria à patologia vai acontecer, ao vivo, no dia 18 de janeiro, às 20h e é EXCLUSIVA para médicos.
                </div>
            
                <div class="mt-2">
                    Vamos compartilhar com você como você pode dominar a ultrassonografia dermatológica de face e saber como atuar neste, que é um dos mercados mais promissores da medicina em 2024.
                </div>
            
                <div class="mt-2">
                    Durante toda a nossa masterclass, você terá acesso à junção da melhor teoria com a vivência prática e se sentirá muito mais seguro para fazer ultrassom de face.
                </div>
            </div>
        </div>
        <div class="col-1 col-sm-1 col-md-2"></div>
    </div>

    <div class="row mt-4">
        <div class="col-1 col-sm-1 col-lg-2"></div>
        <div class="col-10 col-sm-10 col-lg-8">
            
            <div class="row d-inline-flex justify-content-center">
                <div class="col-md-3 cardOutlineRoxo p-3 mt-4 mb-5 removeMbMobile text-center text-white">
                   <div class="font-weight-strong">
                        <img src="./assets/icons/icon1.webp" alt="" class="card-img cardIcons">
                        <div class="mt-3 textCardIcons">
                            ANATOMIA DA FACE
                        </div>
                   </div>
                </div>
    
                <div class="col-md-3 marginCardDesktop cardOutlineRoxo p-3 mt-4 mb-5 removeMbMobile text-center text-white">
                   <div class="font-weight-strong">
                        <img src="./assets/icons/icon2.webp" alt="" class="card-img cardIcons">
                        <div class="mt-3 textCardIcons">
                            CLAREZA PARA AVALIAR PATOLOGIAS
                        </div>
                   </div>
                </div>
    
                <div class="col-md-3 cardOutlineRoxo p-3 mt-4 mb-5 removeMbMobile text-center text-white">
                   <div class="font-weight-strong">
                        <img src="./assets/icons/icon3.webp" alt="" class="card-img cardIcons">
                        <div class="mt-3 textCardIcons">
                            ULTRASSOM DA FACE COM MAIS SEGURANÇA
                        </div>
                   </div>
                </div>
            </div>

            <div class="row d-inline-flex justify-content-center">
                <div class="col-md-3 cardOutlineRoxo p-3 mt-4 mb-5 removeMbMobile text-center text-white">
                   <div class="font-weight-strong">
                        <img src="./assets/icons/icon4.webp" alt="" class="card-img cardIcons">
                        <div class="mt-3 textCardIcons">
                            CASOS PRÁTICOS
                        </div>
                   </div>
                </div>
    
                <div class="col-md-3 marginCardDesktop cardOutlineRoxo p-3 mt-4 mb-5 removeMbMobile text-center text-white">
                   <div class="font-weight-strong">
                        <img src="./assets/icons/icon5.webp" alt="" class="card-img cardIcons">
                        <div class="mt-3 textCardIcons">
                            FAÇA ULTRASSOM DE ALTA RESOLUÇÃO
                        </div>
                   </div>
                </div>
    
                <div class="col-md-3 cardOutlineRoxo p-3 mt-4 mb-5 text-center text-white">
                   <div class="font-weight-strong">
                        <img src="./assets/icons/icon6.webp" alt="" class="card-img cardIcons">
                        <div class="mt-3 textCardIcons">
                            CONTEÚDO EXCLUSIVO
                        </div>
                   </div>
                </div>
            </div>
        </div>
        <div class="col-1 col-sm-1 col-lg-2"></div>
    </div>

    <div class="row">
        <div class="col-1 col-sm-1 col-md-2"></div>
        <div class="col-10 col-sm-10 col-md-8">
            <div class="row p-2 text-white text-center textNormal">
                <div>
                    <i class="font-weight-bolder">
                        Este conteúdo não é ensinado de forma clara nas faculdades ou pós graduações, nem mesmo na residência aprendemos sobre ultrassom de face.
                    </i>
                </div>

                <div class="mt-2">
                    Ter esse conhecimento é a possibilidade de se destacar da maioria e se tornar uma referência médica.
                </div>

                <div class="mt-2">
                    Dominar ultrassom de face é a possibilidade de fornecer diagnósticos mais precisos e evitar riscos desnecessários para o paciente, aprimorando ainda mais a qualidade da assistência médica.
                </div>
            </div>
        </div>
        <div class="col-1 col-sm-1 col-md-2"></div>
    </div>

    <div class="row text-center mt-4">
        <div class="col-1"></div>
        <div class="col-10">
            <?php require('./content/btn_participar.php'); ?>  
        </div>
        <div class="col-1"></div>
    </div>

</div>