<div id="section1">

    <div class="row">
        <div class="col-1 col-lg-2"></div>
        <div class="col-10 col-lg-4">
            <div class="marginSection1Desktop"></div>
            <div class="row" id="rowSection1">
                <div class="col-2 col-sm-2"></div>
                <div class="col-8 col-sm-8 text-center mt-4">
                    <img src="./assets/logo_oficial2.webp" class="card-img logoSection1Mobile">
                </div>
                <div class="col-2 col-sm-2"></div>
            </div>
            
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 text-center mt-4 text-white">
                    <div class="cardOutlineRoxo p-2 font-weight-bolder">
                        EVENTO EXCLUSIVO PARA MÉDICOS
                    </div>
                    <?php require('./content/bloco_data.php'); ?>
        
                    <div class="mt-3 font-weight-light">
                        <span class="textNormal">
                            O passo a passo para você fazer ultrassom de face, dominar este exame e entrar em um dos mercados mais promissores da medicina atual.
                        </span>
                        <br>
                        <div class="mt-3 textExtraSmall hideMobile">
                            Vamos unir a teoria e a prática para te entregar o direcionamento claro para fazer ultrassom de face, sabendo como avaliar patologias e direcionar cosmiatria.
                        </div>
                    </div>
        
                    
                    <div class="mt-4">
                        <a href="#" class="btn btnParticiparInicial" id="btnParticipar">
                            CLIQUE PARA PARTICIPAR
                        </a>
                    </div>
        
                    
                    <div class="cardOutlineRoxo p-2 mt-4 mb-5" style="background-color: black">
                        AO VIVO | 100% ONLINE E GRATUITO
                    </div>
        
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
        <div class="col-1 col-lg-6"></div>
    </div>

</div>