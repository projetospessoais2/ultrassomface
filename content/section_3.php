<div id="section3e5" class="p-5">
    <br><br class="hideMobile">
    <br class="hideMobile">

    <div class="text-white">
        
        <div class="row">
            <div class="col-lg-3"></div>

            <div class="col-md-6 col-lg-3 d-inline-flex justify-content-end align-items-center">
                <div class="text-center">
                    <span class="font-weight-strong lh-1-5 textLarge">
                        PARA QUEM É ESTE EVENTO?
                    </span>
                    <div class="mt-2 textSmall">
                        Toda a nossa Masterclass será focada em assuntos técnicos e conteúdos direcionados para:
                    </div>
                </div>
            </div>
    
            <div class="col-md-6 colListaProfissoes">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10 textBigger colCardListaProfissoes cardOutlineRoxo mt-3 p-2">
                        <div class="mb-2">
                            &nbsp;&nbsp;
                            <i class="fas fa-check purpleIcons"></i>
                            &nbsp;
                            <span class="text-white font-weight-bolder">
                                Radiologistas
                            </span>
                        </div>
                        <div class="mb-2">
                            &nbsp;&nbsp;
                            <i class="fas fa-check purpleIcons"></i>
                            &nbsp;
                            <span class="text-white font-weight-bolder">
                                Ultrassonografistas
                            </span>
                        </div>
                        <div class="mb-2">
                            &nbsp;&nbsp;
                            <i class="fas fa-check purpleIcons"></i>
                            &nbsp;
                            <span class="text-white font-weight-bolder">
                                Dermatologistas
                            </span>
                        </div>
                        <div>
                            &nbsp;&nbsp;
                            <i class="fas fa-check purpleIcons"></i>
                            &nbsp;
                            <span class="text-white font-weight-bolder">
                                Cirurgiões Plásticos
                            </span>
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </div>

        <div class="text-center mt-4">
            <?php require('./content/btn_participar.php'); ?>  
        </div>

    </div>

    <br><br class="hideMobile">
    <br class="hideMobile">
</div>