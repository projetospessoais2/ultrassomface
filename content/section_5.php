<div id="section3e5" class="p-5">

    <div class="row">
        <div class="col-sm-1 col-md-3 col-lg-4"></div>
        <div class="col-sm-10 col-md-6 col-lg-4 text-center mt-4 marginTopSecao5Mobile">
            <img src="./assets/logo_oficial2.webp" class="card-img logoSectionFinal">
            <?php require('./content/bloco_data.php'); ?>

            <div class="mt-4">
                <?php require('./content/btn_participar.php'); ?>
            </div>

        </div>
        <div class="col-sm-1 col-md-3 col-lg-4"></div>
    </div>

</div>