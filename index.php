<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />

        <!-- <link rel="icon" type="image/x-icon" href="assets/favicon.ico"> -->
        <title>Ultrassom da Face | Cadastro | Versão 5 – Dra. Cláudia &amp; Dra. Juliana</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/used.css">
        <!-- <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/form.css">
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/responsividade.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->

    </head>

    <body style="background-color: black !important">
        
    
        <?php require('content/section_1.php'); ?>
        <?php require('content/section_2.php'); ?>
        <?php require('content/section_3.php'); ?>
        <?php require('content/section_4.php'); ?>
        <?php require('content/section_5.php'); ?>

        <?php require('content/formulario.php'); ?>

        <script src="js/activeCampaign.js"></script>

    </body>

    <?php require('default/footer.php'); ?>

</html>